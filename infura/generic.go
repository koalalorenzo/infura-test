package infura

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"

	"github.com/INFURA/go-ethlibs/jsonrpc"
	"gitlab.com/koalalorenzo/infura-test/conf"
)

// PerformRequest will execute the HTTP Post request. I could have used a
// library but for simplicity I am using direclty http package
func PerformRequest(c *conf.Configuration, r *jsonrpc.Request) ([]byte, error) {
	payloadBytes, err := r.MarshalJSON()
	if err != nil {
		log.WithField("error", err).Error("Error preparing payload")
		return nil, err
	}

	body := bytes.NewReader(payloadBytes)
	url := fmt.Sprintf("%s/%s", c.Infura.Endpoint, c.Infura.ProjectID)

	log.WithFields(logrus.Fields{
		"url":  url,
		"body": string(payloadBytes),
	}).Debug("New request")

	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		log.WithFields(logrus.Fields{
			"error":   err.Error(),
			"url":     url,
			"payload": string(payloadBytes),
		}).Error("Error preparing request")
		return nil, err
	}

	// Set here headers. Like auth for Infura (Ex: JWT tokens) or other things
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.WithFields(logrus.Fields{
			"error":   err.Error(),
			"url":     url,
			"payload": string(payloadBytes),
		}).Error("Error making request")
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}
