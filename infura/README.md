# Infura package

This small package includes everything needed to call the Infura API.

`PerformRequest` will perform the HTTP request. This function is used by other
functions like `GetBlock` and `GetTxByBlockNAndIndex` to avoid code repetition

`GetBlock` and `GetTxByBlockNAndIndex` are not caching results, but they are
used by `GetCachedBlock` and `GetCachedGetTxByBlockNAndIndex` to obtain the
value from Infura or use the cached data retrieved previously.
