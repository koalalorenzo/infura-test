package infura

import (
	"github.com/allegro/bigcache"
)

var cache *bigcache.BigCache

func SetupCache(c *bigcache.BigCache) error {
	cache = c
	return nil
}
