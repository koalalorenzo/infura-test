package infura

import (
	"fmt"

	"github.com/INFURA/go-ethlibs/jsonrpc"

	"gitlab.com/koalalorenzo/infura-test/conf"
)

// GetBlock will return a []byte containing a block of a specific height
func GetBlock(c *conf.Configuration, height string) ([]byte, error) {
	log.Debugf("eth_getBlockByNumber %s", height)

	r, err := jsonrpc.MakeRequest(1, "eth_getBlockByNumber", height, false)
	if err != nil {
		return nil, err
	}

	return PerformRequest(c, r)
}

// GetCachedBlock works like GetBlock but uses an internal cache
func GetCachedBlock(c *conf.Configuration, height string) ([]byte, error) {
	if !c.Cache.Enable {
		log.Debug("Cache disabled")
		return GetBlock(c, height)
	}

	switch height {
	case "earliest":
	case "latest":
	case "pending":
		log.Debug("Cannot cache pending, latest or earliest")
		return GetBlock(c, height)
	}

	ckey := fmt.Sprintf("bl-%s", height)
	if entry, err := cache.Get(ckey); err == nil {
		log.WithField("block", height).Debugf("Found in cache")
		return entry, nil
	} else {
		log.WithField("block", height).Debugf("Not found in cache")

		b, err := GetBlock(c, height)
		if err != nil {
			return nil, err
		}

		// Save it into the cache
		go cache.Set(ckey, b)

		return b, err
	}
}
