package infura

import "github.com/sirupsen/logrus"

var log *logrus.Logger

// SetUpLogger is an internal shortcut to set up logging properly
func SetUpLogger(l *logrus.Logger) {
	log = l
}
