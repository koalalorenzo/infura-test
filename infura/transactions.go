package infura

import (
	"fmt"

	"github.com/INFURA/go-ethlibs/jsonrpc"
	"github.com/sirupsen/logrus"

	"gitlab.com/koalalorenzo/infura-test/conf"
)

// GetTxByBlockNAndIndex will return a []byte containing the response from the
// infura JSON RPC request. Check eth_getTransactionByBlockNumberAndIndex for
// more information about the answer.
func GetTxByBlockNAndIndex(c *conf.Configuration, block, txindex string) ([]byte, error) {
	log.Debugf("eth_getTransactionByBlockNumberAndIndex [%s, %s]", block, txindex)

	r, err := jsonrpc.MakeRequest(1, "eth_getTransactionByBlockNumberAndIndex", block, txindex)
	if err != nil {
		return nil, err
	}

	return PerformRequest(c, r)
}

// GetCachedGetTxByBlockNAndIndex works like GetTxByBlockNAndIndex but uses an
// internal cache to avoid multiple calls
func GetCachedGetTxByBlockNAndIndex(c *conf.Configuration, block, txindex string) ([]byte, error) {
	if !c.Cache.Enable {
		log.Debug("Cache disabled")
		return GetTxByBlockNAndIndex(c, block, txindex)
	}

	switch block {
	case "earliest":
	case "latest":
	case "pending":
		log.Debug("Cannot cache pending, latest or earliest")
		return GetTxByBlockNAndIndex(c, block, txindex)
	}

	ckey := fmt.Sprintf("tx-%s[%s]", block, txindex)
	if entry, err := cache.Get(ckey); err == nil {
		log.WithFields(logrus.Fields{
			"block":   block,
			"txindes": txindex,
		}).Debugf("Found in cache")
		return entry, nil

	} else {
		log.WithFields(logrus.Fields{
			"block":   block,
			"txindes": txindex,
		}).Debugf("Not found in cache")

		b, err := GetTxByBlockNAndIndex(c, block, txindex)
		if err != nil {
			return nil, err
		}

		// Save it into the cache
		go cache.Set(ckey, b)

		return b, err
	}
}
