# Server package

This package manages the REST API. It takes care to set up the Logging and
Caching instance for the infura module as well as setting up routes and
serving HTTP server.

It exposes:

- `/blocks/{blockID}` which will be equivalent to `eth_getBlockByNumber`
- `/blocks/{blockID}/txindex/{txindx}` which will be equivalent to `eth_getTransactionByBlockNumberAndIndex`
