package server

import (
	"net/http"

	"github.com/allegro/bigcache"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/urfave/negroni"

	negronilogrus "github.com/meatballhat/negroni-logrus"
	"gitlab.com/koalalorenzo/infura-test/conf"
	"gitlab.com/koalalorenzo/infura-test/infura"
)

var appConf *conf.Configuration
var log *logrus.Logger

// Listen will set up and start and listens for HTTP requests.
// It accepts only the configuration that defines the port/address to listen to.
func Listen(c *conf.Configuration, l *logrus.Logger, cache *bigcache.BigCache) {
	appConf = c
	log = l
	infura.SetUpLogger(l)
	infura.SetupCache(cache)

	// Set up Routing
	rmux := mux.NewRouter()
	rmux.HandleFunc("/", rootHandler)

	pr := rmux.PathPrefix("/blocks").Subrouter()
	pr.Methods("GET").Path("/").HandlerFunc(rootHandler)
	pr.Methods("GET").Path("/{height}").HandlerFunc(blocksHandler)
	pr.Methods("GET").Path("/{height}/tx/{txindex}").HandlerFunc(txByBlockAndIndexHandler)

	// Adding some drinks to make it easier
	n := negroni.New()
	n.Use(negronilogrus.NewCustomMiddleware(logrus.InfoLevel, &logrus.TextFormatter{}, "api"))
	n.UseHandler(rmux)

	// TODO: Docker Healthchecks
	// TODO: k8s healthchecks

	log.Infof("Listening to HTTP request at %s", c.HTTPAddrListen)
	http.ListenAndServe(c.HTTPAddrListen, n)
}
