package server

import (
	"net/http"
)

// Root handler for requets for `/`
func rootHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}
