package server

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/koalalorenzo/infura-test/infura"
)

func blocksHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)

	blockHeight := vars["height"]
	d, err := infura.GetCachedBlock(appConf, blockHeight)

	if err != nil {
		log.WithError(err).Error("Internal Server Error")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"error": "Internal Server Error"}`))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(d)
}
