module gitlab.com/koalalorenzo/infura-test

go 1.12

require (
	github.com/INFURA/go-ethlibs v0.0.0-20190626190020-c6f644462ee9
	github.com/allegro/bigcache v1.2.1
	github.com/gorilla/mux v1.7.3
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/meatballhat/negroni-logrus v0.0.0-20170801195057-31067281800f
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/negroni v1.0.0
)
