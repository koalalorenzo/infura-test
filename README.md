# INFURA Infrastructure Test

This is a take-home test for the INFURA infrastructure team that we would like
you to attempt. You'll find a number of steps to complete below that will
require you to do some coding and configuration; feel free to use the
programming or scripting language(s) you're most comfortable with (although
Python and Go are preferred) and tools, libraries, or frameworks you believe are
best suited to the tasks listed below.

Please submit any code and documentation you write when you feel comfortable with
the result. This test is intended to be completed within a week; should you need
more time, please let us know.

Please also note and let us know how long you worked on the test; this is for
informational purposes only and allows us to make adjustments and improvements
for future applicants (feel free to provide feedback on the test itself too).

## How to build

To build the source code into binaries you can run:

```bash
make build -e GOOS=linux
```

Where `GOOS` is set with the OS you want to target (ex: `darwin`, `linux` etc).

You can also build a docker container by running:

```bash
make docker_build
```

It is suggested to download the latest docker container from the GitLab
registry [here](https://gitlab.com/koalalorenzo/infura-test/container_registry)

## Pipeline

This take-home test has a GitLab Pipeline, for more information about its status
or the results [check the project here](https://gitlab.com/koalalorenzo/infura-test/pipelines) (requires invitation)

The pipeline is capable of:

* Testing the source code
* Building binaries and uploading the results for later downloads
* Building and pushing the docker container with tags based on branch
* Running basic benchmark tests using GitLab CI services

## Environmental Variables

The service requires you to set up the following environmental variables when
running it as a binary or as a Docker container:

* `INFURA_PROJECT_ID`: containing the
  [INFURA Project ID](https://infura.io/register)

Other optional variables are:

* `HTTP_ADDR_LISTEN` (default: `:8080`)
* `LOG_FORMAT` (default: `text`)
* `LOG_LEVEL` (default: `debug`)
* `INFURA_ENDPOINT` (default: `https://mainnet.infura.io/v3`)
* `CACHE_ENABLE` (default: `true`)

## Components

The source code is divided in 3 packages:

* `conf`: that handles the configuration, ideally syncs with Hashicorp Vault for
  secrets handling and to retrieve proper tokens.
* `infura`: acts as an interface between the Infura API and the rest of the code.
  It also implements basic Cache when possible.
* `server`: handles HTTP setup and endpoints with handlers.
* `bin`: not really a package but if you read until here I am happy.

## Benchmarks

Benchmark tests for the source code has been executed using
[Apache Bench](https://en.wikipedia.org/wiki/ApacheBench). Go tests for
benchmarking were considered but the kind of tests we were setting up
were already performed by the modules
([example](https://github.com/smallnest/go-web-framework-benchmark)). What
I wanted to validate is the whole setup, and I preferred to use Apache Bench
for this reason. Please install `ab` before continuing:

* Debian/Ubuntu: `apt install apache2-utils gnuplot`
* macOS: `brew install gnuplot` ; Apple includes `ab` already
* Windows: see Ubuntu (Not tested)

Once the server is up and running (either `make run -e INFURA_PROJECT_ID="123"`
or via binary/docker), you can run the benchmarks like this:

```bash
make benchmark_ab
```

This will create a new directory containing HTML and images (`.png`) files
as well as gnuplot data for more fun! If you want to customize the
server/endpoint to use for testing, you can specify the `AB_ENDPOINT` env
variable. For example:

```bash
make benchmark_ab -e AB_ENDPOINT="http://raspberrypi.local"
```

**Note**: every time you run `benchmark_ab` target, it will clean the `./bench`
directory! Run with caution if you like the results you see 😅

For more details about the commands used check the `Makefile` code.

### Some results

These tests were conducted locally on a MacBook Pro 2016 with 3,1 GHz Intel Core i5
and RAM 16 GB 2133 MHz LPDDR3. The tests running in localhost can't really
provide a real-life experience (a proper test should be executed from a cloud
standard-size VM with proper connection to a similar VM running the server).

#### Not Cached Data

There are some requests that can't be cached, like the ones that are
related to `latest` blocks. These tests shows that the requests from Infura API
server are slowing down a little, but they are still responsive and not
really impacting the stress the server was put under.


Results for `/blocks/latest` (no cache)

![Block requests (no cache)](.docs/latest.png "Block requests (no cache)")

Results for `/blocks/latest/tx/0x0` (no cache)

![Txn requests (no cache)](.docs/latest-tx.png "Transactions requests (no cache)")


#### Cache data

This kind of requests can be cached as we know that their response is immutable.
Therefore we can allow a cache locally that does not require invalidation.

Results for `/blocks/0x5BAD55`

![Block requests](.docs/0x5BAD55.png "Block requests")

Results for `/blocks/0x5BAD55/tx/0x0`

![Txn requests](.docs/0x5BAD55-tx.png "Transactions requests")
