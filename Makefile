GIT_COMMIT_SHORT ?= $(shell git log -1 --pretty=format:"%h")

DOCKER_BASE_IMAGE ?= registry.gitlab.com/koalalorenzo/infura-test
DOCKER_TAG ?= $(GIT_COMMIT_SHORT)
DOCKER_IMAGE ?= $(DOCKER_BASE_IMAGE):$(DOCKER_TAG)

VERSION ?= $(GIT_COMMIT_SHORT)
GOOS ?=
BUILD_OUT ?= build/app_${GOOS}_${VERSION}

AB_ENDPOINT ?= http://localhost:8080

build:
	mkdir -p build
	CGO_ENABLED=0 go build -a -installsuffix cgo -o ${BUILD_OUT} bin/main.go
.PHONY: build

docker_build:
	docker build --compress --force-rm -t "${DOCKER_IMAGE}" .
.PHONY: docker_build

docker_push:
	docker push "${DOCKER_IMAGE}"
.PHONY: docker_push

run:
	go run bin/main.go
.PHONY: run

# Run bin is a shortcut to run the binary previously built by build target.
# Note that we are not specifying the dependency on Build as it will be used
# to quickly test in the pipeline the binaries built
run_bin:
	${BUILD_OUT}
.PHONY: run_bin

test:
	go test -v ./...
.PHONY: test

clean:
	rm -rf build
.PHONY: clean

benchmark_ab:
	rm -rf bench
	mkdir -p bench
	# Getting not cachable requests
	ab -n 1000 -c 10 -g bench/latest.data ${AB_ENDPOINT}/blocks/latest
	sleep 3
	ab -n 1000 -c 10 -g bench/latest-tx.data ${AB_ENDPOINT}/blocks/latest/tx/0x0
	sleep 3
	ab -n 1000 -c 10 -w ${AB_ENDPOINT}/blocks/latest > bench/latest.html
	sleep 3
	ab -n 1000 -c 10 -w ${AB_ENDPOINT}/blocks/latest/tx/0x0 > bench/latest-tx.html
	sleep 3
	# Getting cached requests
	ab -n 1000 -c 50 -g bench/0x5BAD55.data ${AB_ENDPOINT}/blocks/0x5BAD55
	sleep 3
	ab -n 1000 -c 50 -g bench/0x5BAD55-tx.data ${AB_ENDPOINT}/blocks/0x5BAD55/tx/0x0
	sleep 3
	ab -n 1000 -c 50 -w ${AB_ENDPOINT}/blocks/0x5BAD55 > bench/0x5BAD55.html
	sleep 3
	ab -n 1000 -c 50 -w ${AB_ENDPOINT}/blocks/0x5BAD55/tx/0x0 > bench/0x5BAD55-tx.html
ifneq (, $(shell which gnuplot))
	# <3 Making Beautiful Images <3
	gnuplot -e "source='bench/0x5BAD55-tx.data'; output='bench/0x5BAD55-tx.png'" ./gnuplot_ab.plg
	gnuplot -e "source='bench/0x5BAD55.data'; output='bench/0x5BAD55.png'" ./gnuplot_ab.plg
	gnuplot -e "source='bench/latest-tx.data'; output='bench/latest-tx.png'" ./gnuplot_ab.plg
	gnuplot -e "source='bench/latest.data'; output='bench/latest.png'" ./gnuplot_ab.plg
endif
.PHONY: benchmark_ab