FROM golang:1.12 as base

WORKDIR /app
COPY go.mod go.sum /app/
RUN go get
COPY . /app/
RUN make clean build -e BUILD_OUT=/main

# Using scratch for super small docker image
FROM scratch
COPY --from=base /main /bin/app

ENV HTTP_ADDR_LISTEN ":8080"
ENV INFURA_PROJECT_ID ""
ENV INFURA_ENDPOINT "https://mainnet.infura.io/v3"
EXPOSE 8080

# TODO: Docker Healthchecks
# TODO: k8s healthchecks

ENTRYPOINT ["/bin/app"]
CMD