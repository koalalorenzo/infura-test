package main

import (
	"time"

	"github.com/allegro/bigcache"
	"github.com/sirupsen/logrus"

	"gitlab.com/koalalorenzo/infura-test/conf"
	"gitlab.com/koalalorenzo/infura-test/server"
)

func main() {
	// Parsing the configuration from env variables
	c, err := conf.ParseConfiguration()
	if err != nil {
		c.Usage()
		return
	}

	// Create and set up logging
	log := logrus.New()

	switch c.Log.Format {
	case "json":
		log.SetFormatter(&logrus.JSONFormatter{})
	default:
		log.SetFormatter(&logrus.TextFormatter{})
	}

	switch c.Log.Format {
	case "debug":
		log.SetLevel(logrus.DebugLevel)
	case "warn":
		log.SetLevel(logrus.WarnLevel)
	default:
		log.SetLevel(logrus.TraceLevel)
	}

	// Setup Cache
	cache, err := bigcache.NewBigCache(bigcache.DefaultConfig(6 * time.Hour))
	if err != nil {
		log.Panicf("Error setting up bigcache: %s", err)
	}

	log.Info("Infura proxy API server starting...")

	// Start the Server
	server.Listen(c, log, cache)
}
