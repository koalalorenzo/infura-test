package conf

import (
	"encoding/json"
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

// Configuration of the app
type Configuration struct {
	HTTPAddrListen string `split_words:"true" default:":8080"`

	Log struct {
		Format string `split_words:"true" default:"text"`
		Level  string `split_words:"true" default:"debug"`
	}

	Infura struct {
		Endpoint  string `split_words:"true" default:"https://mainnet.infura.io/v3"`
		ProjectID string `split_words:"true" required:"true"`
	}

	Cache struct {
		Enable bool `split_words:"true" default:"true"`
	}
}

// ParseConfiguration will check ENV variables and populate Configuration
func ParseConfiguration() (*Configuration, error) {
	var c Configuration
	err := envconfig.Process("", &c)
	return &c, err
}

// Usage will show to console the usage
func (c *Configuration) Usage() {
	envconfig.Usage("", c)
}

// ToJSON the current configuration
func (c *Configuration) ToJSON() string {
	out, _ := json.MarshalIndent(c, "", "\t")
	return fmt.Sprintf("%s", out)
}
